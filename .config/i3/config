set $mod Mod4

workspace_layout default 
# stacking - tabbed
focus_follows_mouse no

font pango: JetBrainsMono Nerd Font 10


new_window pixel 1
# thin borders
# hide_edge_borders both
gaps inner 2
gaps outer 2

default_border pixel 2
default_floating_border pixel 3


# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# switch/iterate between workspaces
bindsym $mod+Tab workspace next
bindsym $mod+Shift+Tab workspace prev

# toggle picom opacity
bindsym $mod+z exec --no-startup-id fish -c "toggle_opacity"

# switch to workspace
bindcode $mod+10    workspace  $ws1
bindcode $mod+11    workspace  $ws2
bindcode $mod+12    workspace  $ws3
bindcode $mod+13    workspace  $ws4
bindcode $mod+14    workspace  $ws5
bindcode $mod+15    workspace  $ws6
bindcode $mod+16    workspace  $ws7
bindcode $mod+17    workspace  $ws8
bindcode $mod+18    workspace  $ws9
bindcode $mod+19    workspace  $ws10

# switch to workspace with numpad keys
bindcode $mod+87 workspace $ws1
bindcode $mod+88 workspace $ws2
bindcode $mod+89 workspace $ws3
bindcode $mod+83 workspace $ws4
bindcode $mod+84 workspace $ws5
bindcode $mod+85 workspace $ws6
bindcode $mod+79 workspace $ws7
bindcode $mod+80 workspace $ws8
bindcode $mod+81 workspace $ws9
bindcode $mod+90 workspace $ws10

# switch to workspace with numlock numpad keys
bindcode $mod+Mod2+87 workspace $ws1
bindcode $mod+Mod2+88 workspace $ws2
bindcode $mod+Mod2+89 workspace $ws3
bindcode $mod+Mod2+83 workspace $ws4
bindcode $mod+Mod2+84 workspace $ws5
bindcode $mod+Mod2+85 workspace $ws6
bindcode $mod+Mod2+79 workspace $ws7
bindcode $mod+Mod2+80 workspace $ws8
bindcode $mod+Mod2+81 workspace $ws9
bindcode $mod+Mod2+90 workspace $ws10

# move focused container to workspace
bindcode $mod+Shift+10    move container to workspace  $ws1
bindcode $mod+Shift+11    move container to workspace  $ws2
bindcode $mod+Shift+12    move container to workspace  $ws3
bindcode $mod+Shift+13    move container to workspace  $ws4
bindcode $mod+Shift+14    move container to workspace  $ws5
bindcode $mod+Shift+15    move container to workspace  $ws6
bindcode $mod+Shift+16    move container to workspace  $ws7
bindcode $mod+Shift+17    move container to workspace  $ws8
bindcode $mod+Shift+18    move container to workspace  $ws9
bindcode $mod+Shift+19    move container to workspace  $ws10

# move focused container to workspace left/right
bindsym  control+$mod+Left   exec  ~/.config/i3/scripts/switchNext.sh -1
bindsym  control+$mod+Right  exec  ~/.config/i3/scripts/switchNext.sh 1

# move focused container to workspace with numpad keys
bindcode $mod+Shift+Mod2+87 	move container to workspace  $ws1
bindcode $mod+Shift+Mod2+88 	move container to workspace  $ws2
bindcode $mod+Shift+Mod2+89 	move container to workspace  $ws3
bindcode $mod+Shift+Mod2+83 	move container to workspace  $ws4
bindcode $mod+Shift+Mod2+84 	move container to workspace  $ws5
bindcode $mod+Shift+Mod2+85 	move container to workspace  $ws6
bindcode $mod+Shift+Mod2+79 	move container to workspace  $ws7
bindcode $mod+Shift+Mod2+80 	move container to workspace  $ws8
bindcode $mod+Shift+Mod2+81 	move container to workspace  $ws9
bindcode $mod+Shift+Mod2+90 	move container to workspace  $ws10

# move focused container to workspace with numpad keys
bindcode $mod+Shift+87 	 move container to workspace  $ws1
bindcode $mod+Shift+88 	 move container to workspace  $ws2
bindcode $mod+Shift+89 	 move container to workspace  $ws3
bindcode $mod+Shift+83 	 move container to workspace  $ws4
bindcode $mod+Shift+84 	 move container to workspace  $ws5
bindcode $mod+Shift+85 	 move container to workspace  $ws6
bindcode $mod+Shift+79 	 move container to workspace  $ws7
bindcode $mod+Shift+80 	 move container to workspace  $ws8
bindcode $mod+Shift+81 	 move container to workspace  $ws9
bindcode $mod+Shift+90 	 move container to workspace  $ws10

bindsym $mod+r mode "resize"
mode "resize" {
    bindsym  h      resize  shrink  width   10  px  or  10  ppt
    bindsym  j      resize  grow    height  10  px  or  10  ppt
    bindsym  k      resize  shrink  height  10  px  or  10  ppt
    bindsym  l      resize  grow    width   10  px  or  10  ppt
    bindsym  Left   resize  shrink  width   10  px  or  10  ppt
    bindsym  Down   resize  grow    height  10  px  or  10  ppt
    bindsym  Up     resize  shrink  height  10  px  or  10  ppt
    bindsym  Right  resize  grow    width   10  px  or  10  ppt

    bindsym  Return  mode  "default"
    bindsym  Escape  mode  "default"
}

bindsym $mod+Return exec --no-startup-id kitty
# bindsym $mod+Return exec --no-startup-id alacritty
bindsym $mod+q kill
bindsym $mod+Shift+e exec --no-startup-id ~/.config/i3/scripts/powermenu
bindsym $mod+l exec --no-startup-id ~/.config/i3/scripts/blur-lock
bindsym $mod+Shift+c reload
bindsym $mod+Shift+r restart
bindsym $mod+Shift+l exec polybar-msg cmd restart

# bindsym F1 exec --no-startup-id ~/.config/i3/scripts/keyhint-2
# keybinding list in editor:
bindsym $mod+F1 exec --no-startup-id xed ~/.config/i3/keybindings

# Backlight control
#bindsym XF86MonBrightnessUp exec --no-startup-id xbacklight +10 && notify-send "Brightness - $(xbacklight -get | cut -d '.' -f 1)%"
#bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -10 && notify-send "Brightness - $(xbacklight -get | cut -d '.' -f 1)%"
# Backlight setting using dunst osc
bindsym XF86MonBrightnessUp exec --no-startup-id ~/.config/i3/scripts/volume_brightness.sh brightness_up
bindsym XF86MonBrightnessDown exec --no-startup-id ~/.config/i3/scripts/volume_brightness.sh brightness_down

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+b focus up
bindsym $mod+o focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+b move up
bindsym $mod+Shift+o move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+g layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# open new empty workspace
bindsym $mod+Shift+n exec --no-startup-id ~/.config/i3/scripts/empty_workspace

# Multimedia Keys

# volume
# use meta keys without showing osc
#bindsym XF86AudioRaiseVolume exec --no-startup-id amixer -D pulse sset Master 5%+ && pkill -RTMIN+1 i3blocks
#bindsym XF86AudioLowerVolume exec --no-startup-id amixer -D pulse sset Master 5%- && pkill -RTMIN+1 i3blocks
# use meta keys showing osc using dunst
bindsym XF86AudioRaiseVolume exec --no-startup-id ~/.config/i3/scripts/volume_brightness.sh volume_up
bindsym XF86AudioLowerVolume exec --no-startup-id ~/.config/i3/scripts/volume_brightness.sh volume_down

# gradular volume control
bindsym $mod+XF86AudioRaiseVolume exec --no-startup-id amixer -D pulse sset Master 1%+ && pkill -RTMIN+1 i3blocks
bindsym $mod+XF86AudioLowerVolume exec --no-startup-id amixer -D pulse sset Master 1%- && pkill -RTMIN+1 i3blocks

# mute
#bindsym XF86AudioMute exec --no-startup-id amixer sset Master toggle && killall -USR1 i3blocks
# use meta keys showing osc using dunst
bindsym XF86AudioMute exec --no-startup-id ~/.config/i3/scripts/volume_brightness.sh volume_mute

# mic mute toggle
bindsym XF86AudioMicMute exec amixer sset Capture toggle

# audio control
bindsym XF86AudioPlay exec --no-startup-id playerctl play-pause 
# Above line will also work for pausing
bindsym XF86AudioNext exec --no-startup-id playerctl next
bindsym XF86AudioPrev exec --no-startup-id playerctl previous

# Redirect sound to headphones
bindsym $mod+p exec --no-startup-id /usr/local/bin/switch-audio-port

## App shortcuts
bindsym $mod+w exec --no-startup-id /usr/bin/firefox
bindsym $mod+n exec --no-startup-id /usr/bin/nemo
bindsym Print exec --no-startup-id scrot ~/%Y-%m-%d-%T-screenshot.png && notify-send "Screenshot saved to ~/$(date +"%Y-%m-%d-%T")-screenshot.png"

# Power Profiles menu switcher (rofi)
bindsym $mod+Shift+p exec --no-startup-id ~/.config/i3/scripts/power-profiles

##########################################
# configuration for workspace behaviour: #
##########################################

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# use workspaces on different displays:
# where you have to replace VGA-0/HDMI-0 with the names for your displays
# you can get from xrandr command
#workspace $ws1 output VGA-0
#workspace $ws2 output VGA-0
#workspace $ws3 output HDMI-0
#workspace $ws4 output HDMI-0
#workspace $ws5 output HDMI-0

# bind program to workspace and focus to them on startup:
# assign [class="Xfce4-terminal"] $ws1
assign [class="(?i)firefox"] $ws2
assign [class="obsidian"] $ws4
assign [class="steamwebhelper"] $ws3
assign [class="steam"] $ws3
# WM_CLASS(STRING) = "steamwebhelper", "steam"
# assign [class="Thunar"] $ws3
# assign [class="thunderbird"] $ws4
# assign [class="TelegramDesktop"] $ws5

# automatic set focus new window if it opens on another workspace than the current:
for_window [class=Xfce4-terminal] focus
for_window [class=(?i)firefox] focus
for_window [class=Thunar] focus
for_window [class=Nemo] focus
for_window [class=Thunderbird] focus
for_window [class=TelegramDesktop] focus

##############
# compositor #
##############

# exec_always --no-startup-id picom -b
# for custom config:
#exec_always --no-startup-id picom --config  ~/.config/picom.conf

#############################################
# autostart applications/services on login: #
#############################################

exec polybar --reload mybar &>/dev/null & disown
# exec polybar
exec --no-startup-id autorandr docked
exec --no-startup-id xset s off 
# exec_always --no-startup-id (sleep 5 && xset s off)
exec --no-startup-id xset -dpms
exec --no-startup-id setxkbmap -layout 'us,gr' -option 'grp:alt_shift_toggle' 
exec systemctl --user start syncthing.service

#get auth work with polkit-gnome
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1

# dex execute .desktop files + apps using /etc/xdg/autostart.
# when second to i3 a DE is installed or mixed usage of i3 + xfce4 or GNOME
# in this cases better disable dex and use manual starting apps using xdg/autostart
# if enabled you should comment welcome app.
# https://github.com/jceb/dex
#exec --no-startup-id dex -a -s /etc/xdg/autostart/:~/.config/autostart/
exec --no-startup-id dex --autostart --environment i3

# start welcome app
#exec --no-startup-id sh /usr/share/endeavouros/scripts/welcome --startdelay=3 

# num lock activated
exec --no-startup-id numlockx on

# configure multiple keyboard layouts and hotkey to switch (Alt+CAPSLOCK in this example)
#exec --no-startup-id setxkbmap -layout 'us,sk' -variant altgr-intl,qwerty -option 'grp:alt_caps_toggle'

# start conky:
#exec_always --no-startup-id conky

# start a script to setup displays
# uncomment the next line, use arandr to setup displays and save the file as monitor:
exec --no-startup-id ~/.screenlayout/monitor.sh

# set wallpaper
# exec --no-startup-id sleep 2 && nitrogen --restore
# exec --no-startup-id sleep 1 && feh --bg-fill /usr/share/endeavouros/backgrounds/endeavouros-wallpaper.png

# set powersavings for display:
# exec --no-startup-id xset s 480 dpms 600 600 600

# disable power saving (for example if using xscreensaver)
#exec --no-startup-id xset -dpms

# use xautolock to use autosuspend rules for mobile devices
# https://wiki.archlinux.org/title/Session_lock#xautolock
#exec --no-startup-id xautolock -time 60 -locker "systemctl suspend"


# xscreensaver
# https://www.jwz.org/xscreensaver
#exec --no-startup-id xscreensaver --no-splash

# Desktop notifications
# dunst config used ~/.config/dunst/dunstrc
# set alternative config if needed:
#exec --no-startup-id /usr/bin/dunst --config ~/.config/dunst/dunstrc
# may need to run dbus-launch explicitly: 
#exec --no-startup-id dbus-launch /usr/bin/dunst
exec --no-startup-id /usr/bin/dunst
# alternative if you installed aside with XFCE4:
# exec --no-startup-id /usr/lib/xfce4/notifyd/xfce4-notifyd &

# https://github.com/nwg-piotr/autotiling
exec_always --no-startup-id autotiling

# Autostart apps as you like
#exec --no-startup-id sleep 2 && xfce4-terminal
# exec --no-startup-id sleep 7 && firefox https://github.com/endeavouros-team/endeavouros-i3wm-setup/blob/main/force-knowledge.md
#exec --no-startup-id sleep 3 && thunar

###############
# system tray #
###############
# if you do not use dex: exec --no-startup-id dex --autostart --environment i3
# you need to have tray apps started manually one by one:

# start blueberry app for managing bluetooth devices from tray:
#exec --no-startup-id blueberry-tray

# networkmanager-applet
#exec --no-startup-id nm-applet

# clipman-applet
#exec --no-startup-id xfce4-clipman

##################
# floating rules #
##################

# set floating (nontiling) for apps needing it
for_window [class="Yad" instance="yad"] floating enable
for_window [class="Galculator" instance="galculator"] floating enable
for_window [class="Blueberry.py" instance="blueberry.py"] floating enable
for_window [class="main.py" instance="main.py"] floating enable
for_window [class="Thunar"] floating enable
for_window [class="Nemo"] floating enable
for_window [class="Xarchiver"] floating enable

# set floating (nontiling) for special apps
for_window [class="Xsane" instance="xsane"] floating enable
for_window [class="Pavucontrol" instance="pavucontrol"] floating enable
for_window [class="qt5ct" instance="qt5ct"] floating enable
for_window [class="Blueberry.py" instance="blueberry.py"] floating enable
for_window [class="Bluetooth-sendto" instance="bluetooth-sendto"] floating enable
for_window [class="Pamac-manager"] floating enable
for_window [window_role="About"] floating enable

for_window [class="blueman-manager"] floating enable
for_window [class="Blueman-manager"] floating enable
for_window [class="pavucontrol"] floating enable
for_window [class="kooha"] floating enable
for_window [class="obsidian"] floating enable
for_window [class="__main__.py" instance="ProtonUp-Qt"] floating enable
for_window [class="mpv" ] floating enable

# PATH OF EXILE 2
for_window [class="steam_app_2694490"] floating enable
assign [class="steam_app_0361160"] $ws1

# set border of floating window
for_window [class="urxvt"] border pixel 1

# set size of floating window
#for_window [window_role="(?i)GtkFileChooserDialog"] resize set 640 480 #to set size of file choose dialog
#for_window [class=".*"] resize set 640 480 #to change size of all floating windows 

# set position of floating window
#for_window [class=".*"] move position center

######################################
# color settings for bar and windows #
######################################

# set primary Rosé Pine colorscheme colors
set $base           #191724
set $surface        #1f1d2e
set $overlay        #26233a
set $muted          #6e6a86
set $subtle         #908caa
set $text           #e0def4
set $love           #eb6f92
set $gold           #f6c177
set $rose           #ebbcba
set $pine           #31748f
set $foam           #9ccfd8
set $iris           #c4a7e7
set $highlightlow   #21202e
set $highlightmed   #403d52
set $highlighthigh  #524f67

# Teming border and Windows --------------
# target                 title     bg    text   indicator  border
client.focused           $rose     $base $text  $rose      $rose
client.focused_inactive  $text     $base $text  $subtle    $surface
client.unfocused         $text     $base $text  $overlay   $overlay
client.urgent            $text     $base $text  $love      $love
client.placeholder       $base     $base $text  $overlay   $overlay
client.background        $base

#####################################
# Application menu handled by rofi: #
#####################################

## rofi bindings fancy application menu ($mod+d /F9 optional disabled)

bindsym $mod+d exec --no-startup-id rofi -modi drun -show drun \
		-config ~/.config/rofi/rofidmenu.rasi

#bindsym F9 exec --no-startup-id rofi -modi drun -show drun \
#		-config ~/.config/rofi/rofidmenu.rasi

## rofi bindings for window menu ($mod+t /F10 optional disabled)

bindsym $mod+t exec --no-startup-id rofi -show window \
		-config ~/.config/rofi/rofidmenu.rasi

#bindsym F10 exec --no-startup-id rofi -show window \
#		-config ~/.config/rofi/rofidmenu.rasi

## rofi bindings to manage clipboard (install rofi-greenclip from the AUR)

#exec --no-startup-id greenclip daemon>/dev/null
#bindsym $mod+c exec --no-startup-id rofi -modi "clipboard:greenclip print" -show clipboard \
#		-config ~/.config/rofi/rofidmenu.rasi
