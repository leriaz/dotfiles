---@diagnostic disable: undefined-global
require("config.lazy")
require("config.settings")
require("config.mappings")
require("config.lspconf")
