local map = vim.keymap.set
local Snacks = require("snacks")

map("n", "<leader>e", "<cmd>Oil<cr>", { desc = "explore" })
map("n", "<C-S>", "<cmd>silent! update! | redraw<cr>", { desc = "force write" })
map("n", "<leader>d", function() vim.diagnostic.open_float() end, { desc = "float diagnostics" })
map("n", "<leader>r", function() vim.lsp.buf.format() end, { desc = "format" })
map("n", "gd", function() Snacks.picker.lsp_definitions() end, { desc = "definition" })

-- PICKER
-- map("n", "<leader><space>", function() Snacks.picker.smart() end, {desc = "smart"})
map("n", "<leader>ff", function() Snacks.picker.files() end, { desc = "files" })
map("n", "<leader>fb", function() Snacks.picker.buffers() end, { desc = "buffers" })
map("n", "<leader>fp", function() Snacks.picker.projects() end, { desc = "projects" })
map("n", "<leader>fr", function() Snacks.picker.recent() end, { desc = "recent" })
map("n", "<leader>fw", function() Snacks.picker.grep() end, { desc = "grep" })
map("n", "<leader>fh", function() Snacks.picker.help() end, { desc = "help" })
map("n", "<leader>fq", function() Snacks.picker.qflist() end, { desc = "quickfix" })
map("n", "<leader>ft", function() Snacks.picker.colorschemes() end, { desc = "colorschemes" })
---@diagnostic disable-next-line: undefined-field
map("n", "<leader>fT", function() Snacks.picker.todo_comments() end, { desc = "todo_comments" })
---@diagnostic disable-next-line: assign-type-mismatch
map("n", "<leader>fa", function() Snacks.picker.files({ cwd = vim.fn.stdpath("config") }) end, { desc = "config" })

map("n", "<leader>ud", function() Snacks.picker.diagnostics() end, { desc = "diagnostics" })
map("n", "<leader>uD", function() Snacks.picker.diagnostics_buffer() end, { desc = "diagnostics buffer" })
map("n", "<leader>us", function() Snacks.picker.lsp_symbols() end, { desc = "symbols" })
map("n", "<leader>ur", function() Snacks.picker.lsp_references() end, { desc = "references" })
map("n", "<leader>uh", function() vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled()) end,
  { desc = "inlay hints" })
map("n", "<leader>un", function() Snacks.notifier.show_history() end, { desc = "notifications" })
map("n", "<leader>ug", function() Snacks.lazygit() end, { desc = "lazygit" })
map("n", "<leader>ub", function() Snacks.git.blame_line() end, { desc = "blame" })


map({ "n", "x" }, "gW", function() Snacks.picker.grep_word() end, { desc = "grep word" })

-- CENTER SCROLL
map("n", "<C-d>", "<C-d>zz")
map("n", "<C-u>", "<C-u>zz")

-- CENTER SEARCH RESULT
map("n", "n", "nzzzv")
map("n", "N", "Nzzzv")

-- TOGGLE LAST BUFFER
map("n", "<bs>", "<c-^>zz", { silent = true, noremap = true })

-- WINDOW
map("n", "<C-H>", "<C-w>h", { desc = "focus left" })
map("n", "<C-J>", "<C-w>j", { desc = "focus down" })
map("n", "<C-K>", "<C-w>k", { desc = "focus up" })
map("n", "<C-L>", "<C-w>l", { desc = "focus down" })
map("n", "<C-Left>", "<cmd>vertical resize -2<cr>", { desc = "resize w+" })
map("n", "<C-Right>", "<cmd>vertical resize +2<cr>", { desc = "resize w-" })
map("n", "<C-Up>", "<cmd>resize +2<cr>", { desc = "resize v+" })
map("n", "<C-Down>", "<cmd>resize -2<cr>", { desc = "resize v-" })
map("n", "<leader>q", "<cmd>confirm q<cr>", { desc = "close window" })

-- QUICKFIX
-- map("n", "<C-down>", "<cmd>cnext<cr>", {desc = "qflist next"})
-- map("n", "<C-up", "<cmd>cprev<cr>", {desc = "qflist prev"})
