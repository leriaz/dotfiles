return {
  {
    "echasnovski/mini.icons",
    opts = {},
    config = function()
      require('mini.icons').setup()
      require('mini.icons').mock_nvim_web_devicons()
    end
  },
  {
    'echasnovski/mini.statusline',
    version = false,

    opts = {}
  },
  {
    'folke/which-key.nvim',
    event = 'VeryLazy',
    opts = {
      icons = {
        mappings = false
      },
      layout = {
        width = { min = 20 },
        spacing = 1
      },
      win = {
        padding = { 0, 0 },
        title_pos = "center"
      },
      preset = "helix"
    }
  },
  {
    "mvllow/modes.nvim",
    cond = false,
    opts = {
      set_number = true,
    }
  },
  {
    "folke/tokyonight.nvim",
    lazy = false,
    priority = 1000,
    opts = {},
  },
}

