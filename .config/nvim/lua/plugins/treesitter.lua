return {
  {
    'nvim-treesitter/nvim-treesitter',
    config = function()
      local configs = require("nvim-treesitter.configs")
      configs.setup({
        ensure_installed = { "lua", "go", "vim", "javascript", "html", "svelte", "typescript", "css" },
        sync_install = false,
        highlight = { enable = true },
        indent = { enable = true },
      })
    end
  },
  {
    'nvim-treesitter/nvim-treesitter-context',
    opts = {
      enable = true,
      max_lines = 1,
      line_number = true
    }
  },

  {
    "nvim-treesitter/nvim-treesitter-textobjects",
    init = function()
    end,
  },
}
