if status is-interactive
    # Commands to run in interactive sessions can go here
end

alias ls='lsd' 
alias cat='bat'

alias dotc='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias dotg='lazygit --git-dir=$HOME/.cfg/ --work-tree=$HOME'

set -gx EDITOR nvim
set -g fish_greeting ""

zoxide init fish | source
fish_config theme choose "Rosé Pine"

# maybe not??
source "$HOME/.cargo/env.fish"
