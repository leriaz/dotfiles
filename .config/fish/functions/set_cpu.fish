function set_cpu
    # Default max frequency
    set -l max_freq "3.5GHz"

    # Check if an argument was provided
    if test (count $argv) -gt 0
        # Get the first argument
        set -l input $argv[1]

        # Check if the input already ends with GHz
        if string match -q '*GHz' $input
            set max_freq $input
        else
            # Append GHz if not already present
            set max_freq "$input"GHz
        end
    end

    # Set the CPU frequency using sudo
    sudo cpupower frequency-set -u $max_freq

    echo "Max CPU frequency set to $max_freq"
end
