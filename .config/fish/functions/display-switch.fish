function display-switch --description 'Switch between laptop and external display configurations'
    # Get the display names
    set laptop (xrandr | string match -r "^(eDP-|LVDS-)\S+" | string split ' ')[1]
    set hdmi (xrandr | string match -r "^HDMI-\S+" | string split ' ')[1]

    # Check if displays are detected
    if test -z "$laptop" -o -z "$hdmi"
        echo "Error: Could not detect display outputs."
        echo "Detected outputs:"
        xrandr --query
        return 1
    end

    switch $argv[1]
        case laptop
            # Turn off HDMI and enable laptop display
            xrandr --output $hdmi --off \
                   --output $laptop --primary --mode 1920x1080 --rate 120
            polybar-msg cmd restart
            echo "Switched to laptop display"

        case external
            # Turn off laptop and enable HDMI display
            xrandr --output $laptop --off \
                   --output $hdmi --primary --mode 2560x1440 --rate 120
            polybar-msg cmd restart
            echo "Switched to external display"

        case '*'
            echo "Usage: display-switch [laptop|external]"
            echo "  laptop   - Switch to laptop display only (1920x1080 @ 120Hz)"
            echo "  external - Switch to external display only (2560x1440 @ 120Hz)"
            return 1
    end
end
