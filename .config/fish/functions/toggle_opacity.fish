function toggle_opacity

  set PICOM_CONF "$HOME/.config/picom/picom.conf"

  set OPACITY1 "0.7;"
  set OPACITY2 "1.0;"

  set CURRENT_OPACITY (grep "inactive-opacity =" $PICOM_CONF | awk '{print $3}')

  if test "$CURRENT_OPACITY" = "$OPACITY1"
    set NEW_OPACITY $OPACITY2
  else
    set NEW_OPACITY $OPACITY1
  end

  # echo $CURRENT_OPACITY
  # echo $NEW_OPACITY

  sed -i "s/inactive-opacity = [0-9]*\.[0-9]*;/inactive-opacity = $NEW_OPACITY/" $PICOM_CONF

  # pkill picom
  # picom --config $PICOM_CONF & 
end
