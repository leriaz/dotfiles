function sync_theme
  # Get current theme from GTK3 settings
    set theme_name (grep "gtk-theme-name" ~/.config/gtk-3.0/settings.ini | cut -d'=' -f2)
    
    if test -z "$theme_name"
        echo "Error: Could not find GTK theme name in settings.ini"
        return 1
    end
    
    # Check both user and system theme directories
    set theme_path
    if test -d ~/.themes/$theme_name/gtk-4.0
        set theme_path ~/.themes/$theme_name/gtk-4.0
    else if test -d /usr/share/themes/$theme_name/gtk-4.0
        set theme_path /usr/share/themes/$theme_name/gtk-4.0
    else
        echo "Error: Theme directory not found for $theme_name"
        return 1
    end
    
    # Create ~/.config/gtk-4.0 if it doesn't exist
    mkdir -p ~/.config/gtk-4.0

    # Remove existing files/symlinks in gtk-4.0 EXCEPT settings.ini
    for file in ~/.config/gtk-4.0/*
        if test (basename $file) != "settings.ini"
            rm -rf $file
        end
    end
    
    # Create symlinks for all files in theme directory
    for file in $theme_path/*
        set basename (basename $file)
        # Skip creating symlink if it's settings.ini
        if test $basename != "settings.ini"
            ln -s $file ~/.config/gtk-4.0/
        end
    end
    
    echo "Successfully linked GTK4 theme files for theme: $theme_name"
    echo "Theme source: $theme_path"
end
