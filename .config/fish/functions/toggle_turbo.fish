function toggle_turbo
    set turbo_file "/sys/devices/system/cpu/intel_pstate/no_turbo"
    if test -f $turbo_file
        set current_state (cat $turbo_file)
        if test $current_state -eq 0
            echo "Disabling Turbo..."
            echo "1" | sudo tee $turbo_file > /dev/null
            echo "Turbo disabled."
        else
            echo "Enabling Turbo..."
            echo "0" | sudo tee $turbo_file > /dev/null
            echo "Turbo enabled."
        end
    else
        echo "Error: Turbo control file not found."
        return 1
    end
end
