import requests
import os

def delete_old_deployments(account_id, project_name, api_token, keep_latest=5):
    """
    Delete old deployments for a specific Cloudflare Pages project
    """
    headers = {
        "Authorization": f"Bearer {api_token}",
        "Content-Type": "application/json"
    }
    
    # Get all deployments
    url = f"https://api.cloudflare.com/client/v4/accounts/{account_id}/pages/projects/{project_name}/deployments"
    response = requests.get(url, headers=headers)
    
    if not response.ok:
        raise Exception(f"Failed to get deployments: {response.text}")
    
    deployments = response.json()['result']
    deployments_to_delete = deployments[keep_latest:]  # Keep the latest N deployments
    
    print(f"Total deployments: {len(deployments)}")
    print(f"Deployments to delete: {len(deployments_to_delete)}")
  
    # Delete old deployments
    for deployment in deployments_to_delete:
        delete_url = f"{url}/{deployment['id']}"
        delete_response = requests.delete(delete_url, headers=headers)
        
        if delete_response.ok:
            print(f"Deleted deployment {deployment['id']} from {deployment['created_on']}")
        else:
            print(f"Failed to delete deployment {deployment['id']}: {delete_response.text}")

# Usage
account_id = "4c8f8ef73f203c8e1310a80ff49965fa"
project_name = "leriazweb"
api_token = "5PSB5_tJknAP7_i0p1wII1-lTHOryF8XVTD-hQSQ"

delete_old_deployments(account_id, project_name, api_token)
